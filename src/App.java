import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> orderList = new ArrayList<>();
        // subTask3
        Order order1 = new Order(1, "Tam Nguyen", 500000, new Date(), true,
                new String[] { "Item 1", "Item 2" });
        Order order2 = new Order(2, "Hoai Nam", 1000000, new Date(), false);
        Order order3 = new Order(3, "Linh Le", 750000, new Date());
        Order order4 = new Order(4, "Phu Pham");

        orderList.add(order1);
        orderList.add(order2);
        orderList.add(order3);
        orderList.add(order4);

        for (Order order : orderList) {
            order.printOrder();
        }

    }

    // subTask1
    public static class Order {
        private int id;
        private String customerName;
        private long price;
        private Date orderDate;
        private boolean confirm;
        private String[] items;

        // subTask2
        public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
            this.id = id;
            this.customerName = customerName;
            this.price = price;
            this.orderDate = orderDate;
            this.confirm = confirm;
            this.items = items;
        }

        public Order(int id, String customerName, long price, Date orderDate, boolean confirm) {
            this.id = id;
            this.customerName = customerName;
            this.price = price;
            this.orderDate = orderDate;
            this.confirm = confirm;
            this.items = new String[0];
        }

        public Order(int id, String customerName, long price, Date orderDate) {
            this.id = id;
            this.customerName = customerName;
            this.price = price;
            this.orderDate = orderDate;
            this.confirm = false;
            this.items = new String[0];
        }

        public Order(int id, String customerName) {
            this.id = id;
            this.customerName = customerName;
            this.price = 0;
            this.orderDate = new Date();
            this.confirm = false;
            this.items = new String[0];
        }

        // subTask4
        public void printOrder() {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd'-'MMMM'-'yyyy", new Locale("vi", "VN"));
            String formattedOrderDate = dateFormat.format(orderDate);
            NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("vi-VN"));
            String formattedPrice = currencyFormat.format(price);

            System.out.println("Price: " + formattedPrice);
            System.out.println("Order ID: " + id);
            System.out.println("Customer Name: " + customerName);
            System.out.println("Price: " + formattedPrice + " VND");
            System.out.println("Order Date: " + formattedOrderDate);
            System.out.println("Confirmed: " + (confirm ? "Yes" : "No"));
            System.out.println("Items:");
            for (String item : items) {
                System.out.println("- " + item);
            }
            System.out.println();
        }

    }
}
